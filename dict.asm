%include "lib.inc"

section .text

global find_word

; rdi - указатель на нуль-терминированную строку
; rsi - указатель на начало словаря
find_word:
    push r13
    push r14
    mov r14, rsi
    mov r13, rdi
    .cycle:
        mov rdi, r13
        lea rsi, [r14 + 8]
        call string_equals
        test rax, rax
        jz .next
        lea rax, [r14 + 8]
        pop r14
        pop r13
        ret
    .next:
        mov r14, [r14]
        test r14, r14
        jz .not_find
        jmp .cycle
    .not_find:
        pop r14
        pop r13
        xor rax, rax
        ret
