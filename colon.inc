%define i 0
%macro colon 2
    %2:
    dq i
    db %1, 0
    %define i %2
%endmacro