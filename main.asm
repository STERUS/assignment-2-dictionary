%define BUF_SIZE 256
%macro print_stringNL 0
    call print_string
    call print_newline
%endmacro

%macro print_errorNL 0
    call print_error
    call print_newline
%endmacro


global _start

%include "lib.inc"
%include "dict.inc"

section .bss
search: resb BUF_SIZE

section .rodata
error_message: db "Buffer overflow, sorry", 0
not_fine_message: db "Sorry, key is not found :-(", 0
fine_message: db "Here is your value:", 0

%include "words.inc"


section .text

_start:

    mov rdi, search
    mov rsi, BUF_SIZE
    call read_string


    test rax, rax
    jz .error_msg

    mov rdi, search
    mov rsi, first_word
    call find_word

    test rax, rax
    jz .not_find
    
    push rax
    mov rdi, fine_message
    print_stringNL
    mov rdi, [rsp]
    call string_length

    add rax, [rsp] ; добавим длину ключа плюс 1, чтобы перейти к value
    inc rax
    add rsp, 8
    
    mov rdi, rax
    print_stringNL
    xor rdi, rdi
    call exit
.not_find:
    mov rdi, not_fine_message
    jmp .error
.error_msg:
    mov rdi, error_message
.error:
    print_errorNL
    mov rdi, 1
    call exit
