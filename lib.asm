global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
%define STD_OUT 1
%define STD_ERROR 2


section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    .counter:
        cmp byte[rdi], 0
        je .end
        inc rdi
        jmp .counter
    .end:
        sub rdi, rax
        mov rax, rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, STD_OUT
    pop rsi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    mov rsi, rdi
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, STD_ERROR
    pop rsi
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1
    push rdi
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    add rsp, 8
    ret
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push 0
    push 0
    push 0
    mov r8, 20

    mov rax, rdi
    .start: 

        xor rdx, rdx
        mov rdi, 0xa
        div rdi
        add rdx, 0x30
        mov [rsp + r8 + 2], dl
        dec r8

        test rax, rax
        
        jnz .start
    .end: 
        lea r8, [r8 + rsp + 3]
        mov rdi, r8
        call print_string
        add rsp, 24
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rax, 0
    jl .negative
    mov rdi, rax
    call print_uint
    ret
    .negative:
        push rax
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .start:
        mov al, byte[rdi]
        cmp al, byte[rsi]
        jne .bad
        cmp al, 0
        je .ok
        inc rdi
        inc rsi
        jmp .start
    .ok:
        mov rax, 1
        ret
    .bad:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    jne .ok
    xor rax, rax
    add rsp, 8
    ret
    .ok:
        pop rax
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r13
    push r12
    mov r12, rdi
    push r12
    mov r13, rsi
    .start:
        call read_char
        cmp rax, ' '
        je .start
        cmp rax, `\t`
        je .start
        cmp rax, `\n`
        je .start
    .counter:
        cmp r13, 0
        jbe .bad
        cmp rax, 0
        je .ok
        cmp al, ' '
        je .ok
        cmp al, `\t`
        je .ok
        cmp al, `\n`
        je .ok
        
        dec r13
        mov [r12], al
        inc r12
        call read_char
        jmp .counter
    .bad:
        pop r12
        pop r12
        pop r13
        xor rax, rax
        ret
    .ok:
        mov byte[r12], 0
        inc r12
        mov r13, r12
        pop r12
        mov rdx, r13
        sub rdx, r12
        dec rdx
        mov rax, r12
        pop r12
        pop r13
        ret
 
 read_string:
    push r13
    push r12
    mov r12, rdi
    push r12
    mov r13, rsi
    .start:
        call read_char
        cmp rax, ' '
        je .start
        cmp rax, `\t`
        je .start
        cmp rax, `\n`
        je .start
    .counter:
        cmp r13, 0
        jbe .bad
        cmp rax, 0
        je .ok
        cmp al, `\n`
        je .ok
        
        dec r13
        mov [r12], al
        inc r12
        call read_char
        jmp .counter
    .bad:
        pop r12
        pop r12
        pop r13
        xor rax, rax
        ret
    .ok:
        mov byte[r12], 0
        inc r12
        mov r13, r12
        pop r12
        mov rdx, r13
        sub rdx, r12
        dec rdx
        mov rax, r12
        pop r12
        pop r13
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor r8, r8
    xor r10, r10
    .start: 
        mov r8b, byte[rdi]
        inc rdi
        cmp r8b, '0'
        jb .end
        cmp r8b, '9'
        ja .end
        imul rax, 10
        add rax, r8
        sub rax, 0x30

        inc r10
        jmp .start
    .end:
        mov rdx, r10
        ret
        
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .negative
    cmp byte[rdi], '+'
    je .positive
    call parse_uint
    jmp .end
    .negative: 
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        inc rdx
        neg rax
        jmp .end
    .positive:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        inc rdx
    .end:
        ret  

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rbx
    xor rbx, rbx
    .start: 
        cmp rdx, 0
        jbe .B
        cmp byte[rdi], 0
        je .A
        mov bl, byte[rdi]
        mov [rsi], bl
        inc rdi
        inc rsi
        dec rdx
        jmp .start
    .A:
        mov [rsi], byte(0)
        pop rbx
        mov rax, rdi
        pop rdi
        sub rax, rdi
        ret
    .B:
        xor rax, rax
        pop rbx
        pop rdi
        ret
