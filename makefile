.PHONY: clean
.PHONY: test

ASM = nasm
PYTHON = python3
ASM_FLAGS = -f elf64
LD = ld

out/%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

out/main.o: lib.inc words.inc dict.inc main.asm
	$(ASM) $(ASM_FLAGS) -o $@ main.asm

lab2: out/main.o out/lib.o out/dict.o
	ld -o $@ out/main.o out/lib.o out/dict.o

test: 
	$(PYTHON) test.py

clean:
	rm out/*.o