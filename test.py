import subprocess


meta1 = 20
testPassed = 0

file = "./lab2"
prefix = b'Here is your value:\n'
postfix = b'\n'
bad = b"Sorry, key is not found :-("
fault = b"Buffer overflow, sorry"

def out_red(text):
    print("\033[31m{}\033[0m".format(text))

def out_green(text):
    print("\033[32m{}\033[0m".format(text))


tests = [
    (b"short", prefix + b"a" + postfix, b''),
    (b"short_real", prefix +  b"" + postfix, b''),
    (b"long", prefix +  b'asdasdd999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999ajlkdjaslkdshldk' + postfix, b''),
    (b"third word", prefix + b"third word explanation" + postfix, b''),
    (b"asdad", b'\n', bad),
    (b"", prefix + b'nothing' + postfix, b''), 
    (b"9" * 255, b'\n', bad),
    (b"9" * 256, b'\n', fault)
    ]
print('\n' ,'-' * meta1,   "Test's Started", '-' * meta1, '\n')

for i in  range(len(tests)):
    print('\n', "-" * meta1, "TEST", i + 1, "-" * meta1, '\n', sep=" ")
    result = subprocess.run([file], input=tests[i][0], capture_output=True)
    print("Input = ", tests[i][0], '\n')
    print(" : Expected standart output = ", tests[i][1], "\n : Expected error output = ", tests[i][2], '\n')
    output = result.stdout
    error = result.stderr
    if (output == tests[i][1] and error == tests[i][2]):
        out_green("\tOK")
        testPassed += 1
    else:
        out_red("\tWRONG")

if testPassed == len(tests):
    out_green("\nGood, all tests passed!\n")
else:
    out_red("\nNot all tests were passed\n")